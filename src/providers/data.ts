import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class Data {

	public opciones:any = {};

	constructor(private storage: Storage) {
		this.storage.get('precios').then((val) => {
			if(val != null) {
				this.opciones = val;
			}else{
				this.opciones = {lona: 10, vinil: 12, diseno: 5, troquelado: 7, tornillos: 2, pega: 2, silicon: 3, laminador: 5};
			}
		});
	}

	set(precios:any){
		this.storage.set('precios', precios);
		this.opciones = precios;
	}

	get():any{		
		return this.opciones;
	}


}
