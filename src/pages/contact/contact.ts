import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ToastController } from 'ionic-angular';
import { Data } from '../../providers/data';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})


export class ContactPage {

	public precios:any = {};
	public loading: boolean;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, private data: Data) {
    this.precios = this.data.get();
    console.log(this.precios);
  }  

  onSubmit(){
  	this.loading = true;
  	this.data.set(this.precios);

    const toast = this.toastCtrl.create({
      message: 'Guardado',
      duration: 2000
    });
    toast.present();

  	this.loading = false;
  }

}
