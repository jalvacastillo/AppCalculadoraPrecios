import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { Data } from '../../providers/data';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

	public producto: any = {};
    public loading = false;

    public opciones:any = {};
    public total:number = 0;

	constructor(public navCtrl: NavController, private data: Data) {
		this.producto.imprecion = 1;
		this.producto.negociar = 0;
		this.producto.material = 'Lona';
	}

	onSubmit(){
		this.opciones = this.data.get();

		this.total = 0;

		this.producto.metrosCuadrados = (this.producto.ancho * this.producto.alto);

		if (this.producto.material == 'Lona') {
			this.producto.precio = this.opciones.lona;
		} else {
			this.producto.precio = this.opciones.vinil;
		}

		this.total += 1 * (this.producto.metrosCuadrados * this.producto.precio);

		if (this.producto.metrosCuadrados < 1) {
			this.producto.negociar = 3;
		}else if (this.producto.metrosCuadrados == 1) {
			this.producto.negociar = 0;
		}else {
			this.producto.negociar = 2;			
		}

		// Si no es solo impreción
		if (!this.producto.imprecion) {

			this.total += 1 * this.opciones.diseno;
			
			if (this.producto.troquelado) {
				console.log(this.opciones.troquelado);
				this.total += 1 * (this.opciones.troquelado * this.producto.metrosCuadrados);
			}

			if (this.producto.pvc) {
				if (this.producto.metrosCuadrados <= 1) { 
					this.total += 1 * 6;
				} else {
					this.total += 1 * 15;
				}
			}

			if (this.producto.tornillos) { 
				this.total += 1 * this.opciones.tornillos;
			}

			if (this.producto.pega) { 
				this.total += 1 * this.opciones.pega;
			}

			if (this.producto.silicon) { 
				this.total += 1 * this.opciones.silicon;
			}

			if (this.producto.laminador) { 
				this.total += 1 * (this.opciones.laminador * this.producto.metrosCuadrados)
			}

		} 


		console.log(this.producto);
		console.log(this.opciones);
		console.log(this.total);

	}

}
